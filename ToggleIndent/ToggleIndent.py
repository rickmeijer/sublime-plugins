import sublime, sublime_plugin

class ToggleIndentCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		if self.view.settings().get('translate_tabs_to_spaces'):
			self.view.run_command('unexpand_tabs')
			self.view.settings().set('tab_size', self.view.settings().get('toggleIndent.tabs'))
		else:
			self.view.settings().set('tab_size', self.view.settings().get('toggleIndent.spaces'))
			self.view.run_command('expand_tabs', {"set_translate_tabs": True})