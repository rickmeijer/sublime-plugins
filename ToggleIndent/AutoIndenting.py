import sublime, sublime_plugin, os

class Autoindenting(sublime_plugin.EventListener):
	# Run ST's 'expand_tabs' command when opening a file,
	# only if there are any tab characters in the file
	# converts tabs to space on save
	# Converts spaces to tabs on open
	def on_load(self, view):
		if view.settings().get('autoindent') == 1:
			view.run_command('unexpand_tabs')
			view.run_command('set_setting', {"setting": "tab_size", "value": 4});

	def on_pre_save(self, view):
		if view.settings().get('autoindent') == 1:
			edit = view.begin_edit()
			view.run_command('set_setting', {"setting": "tab_size", "value": 2});
			view.run_command('expand_tabs', {"set_translate_tabs": True})
			view.end_edit(edit)
	
	def on_post_save(self, view):
		if view.settings().get('autoindent') == 1:
			edit = view.begin_edit()
			view.run_command('unexpand_tabs')
			view.run_command('set_setting', {"setting": "tab_size", "value": 4});
			view.end_edit(edit)
