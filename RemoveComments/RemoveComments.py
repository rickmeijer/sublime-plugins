import sublime, sublime_plugin

class RemoveCommentsCommand(sublime_plugin.TextCommand):
	def run(self, view):	
		edit = self.view.begin_edit()
		region = self.view.find('(^[\s]*?\/\/.*)|(/\*[\s\S]+?\*/)')
		self.view.erase(edit, region)
		self.view.end_edit(edit)